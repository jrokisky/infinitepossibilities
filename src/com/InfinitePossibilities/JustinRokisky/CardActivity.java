package com.InfinitePossibilities.JustinRokisky;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class CardActivity extends Activity {
	private Button buttBlue;
	private Button buttYellow;
	private Button buttGreen;
	private Button buttWhite;
	
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.color_cards);
        
        //These 4 ClickListeners are for selecting a new Diet card and reinit data        
        buttBlue = (Button)findViewById(R.id.buttonBlue); 
        buttBlue.setOnClickListener(new OnClickListener() { 
            public void onClick(View v) {
            	SharedPreferences settings = getSharedPreferences("stats", 0);
            	Editor edit = settings.edit();
            	edit.clear();
            	edit.putString("Card", "Blue");
            	edit.commit();
                finish();
            }
        });
        buttYellow = (Button)findViewById(R.id.buttonYellow); 
        buttYellow.setOnClickListener(new OnClickListener() { 
            public void onClick(View v) {
            	SharedPreferences settings = getSharedPreferences("stats", 0);
            	Editor edit = settings.edit();
            	edit.clear();
            	edit.putString("Card", "Yellow");
            	edit.commit();
                finish();
            }
        });
        buttGreen = (Button)findViewById(R.id.buttonGreen); 
        buttGreen.setOnClickListener(new OnClickListener() { 
            public void onClick(View v) {
            	SharedPreferences settings = getSharedPreferences("stats", 0);
            	Editor edit = settings.edit();
            	edit.clear();
            	edit.putString("Card", "Green");
            	edit.commit();
                finish();
            }
        });
        buttWhite = (Button)findViewById(R.id.buttonWhite); 
        buttWhite.setOnClickListener(new OnClickListener() { 
            public void onClick(View v) {
            	SharedPreferences settings = getSharedPreferences("stats", 0);
            	Editor edit = settings.edit();
            	edit.clear();
            	edit.putString("Card", "White");
            	edit.commit();
                finish();
            }
        });
        
    }

}
