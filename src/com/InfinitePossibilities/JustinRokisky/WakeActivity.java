package com.InfinitePossibilities.JustinRokisky;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class WakeActivity extends Activity {
	private Button buttWake; //for setting the time that the day began
	private SharedPreferences settings;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.wake_screen);
    	
    	buttWake = (Button)findViewById(R.id.button_wake);
        buttWake.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                setWake();
                settings = getSharedPreferences("stats", 0);
            	Editor edit = settings.edit();
            	edit.putBoolean("Wake", true);
            	edit.commit();
                Intent intent = new Intent(WakeActivity.this, InfinitePossibilitesActivity.class);
                startActivity(intent);
                finish();
            }
        });
        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;    	
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {	
    	switch(item.getItemId()) {
    	case R.id.menu_itemCards:
    		startActivity(new Intent(this,CardActivity.class));
    		break;
    	}
    	return true;
    }

    
    /**
     * Schedules bead rotations for the day and other diet events
     */
    private void setWake() {
    	//Create an offset from the current time in which the alarm will go off.
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 5);
        Calendar midday = Calendar.getInstance();
        Calendar night = Calendar.getInstance();
        
        night.set(Calendar.HOUR_OF_DAY, 19);
        night.set(Calendar.MINUTE, 0);
        night.set(Calendar.SECOND, 0);
        
        Calendar beginEat = Calendar.getInstance();
        beginEat.set(Calendar.HOUR_OF_DAY, 11);
        beginEat.set(Calendar.MINUTE, 0);
        beginEat.set(Calendar.SECOND, 0);
        
        Calendar remindEat = Calendar.getInstance();
        remindEat.set(Calendar.HOUR_OF_DAY, 17);
        remindEat.set(Calendar.MINUTE, 30);
        remindEat.set(Calendar.SECOND, 0);
        
        Calendar endEat = Calendar.getInstance();
        endEat.set(Calendar.HOUR_OF_DAY, 18);
        endEat.set(Calendar.MINUTE, 0);
        endEat.set(Calendar.SECOND, 0);
        
        long spacing = 7200000;
        
        settings = getSharedPreferences("stats", 0);
        String color = settings.getString("Card", "Blue");
        if(color.compareTo("Blue") == 0) {
        	midday.set(Calendar.HOUR_OF_DAY, 14);
        	midday.set(Calendar.MINUTE, 0);
        	midday.set(Calendar.SECOND, 0);
        } else if(color.compareTo("Green") == 0) {
        	midday.set(Calendar.HOUR_OF_DAY, 15);
        	midday.set(Calendar.MINUTE, 0);
        	midday.set(Calendar.SECOND, 0);
        } else if(color.compareTo("White") == 0) {
        	midday.set(Calendar.HOUR_OF_DAY, 16);
        	midday.set(Calendar.MINUTE, 0);
        	midday.set(Calendar.SECOND, 0);
        } else {
        	spacing = 5400000;
        	midday.set(Calendar.HOUR_OF_DAY, 16);
        	midday.set(Calendar.MINUTE, 0);
        	midday.set(Calendar.SECOND, 0);
        }
        
        //Create a new PendingIntent and add it to the AlarmManager
        Intent intent = new Intent(this, ChangeBeadsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
            1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for midday bead rotation
        Intent intent2 = new Intent(this, ChangeBeadsActivity.class);
        PendingIntent pendingIntent2 = PendingIntent.getActivity(this,
            2, intent2, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for night bead rotation
        Intent intent3 = new Intent(this, ChangeBeadsActivity.class);
        PendingIntent pendingIntent3 = PendingIntent.getActivity(this,
            3, intent3, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for begin Eat
        Intent intent4 = new Intent(this, BeginEatActivity.class);
        PendingIntent pendingIntent4 = PendingIntent.getActivity(this,
            4, intent4, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for remind Eat
        Intent intent5 = new Intent(this, RemindEatActivity.class);
        PendingIntent pendingIntent5 = PendingIntent.getActivity(this,
            5, intent5, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for end Eat
        Intent intent6 = new Intent(this, EndEatActivity.class);
        PendingIntent pendingIntent6 = PendingIntent.getActivity(this,
            6, intent6, PendingIntent.FLAG_CANCEL_CURRENT);
        
        AlarmManager am =
            (AlarmManager)getSystemService(Activity.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), spacing,pendingIntent);
        
        Calendar cur = Calendar.getInstance();
        long time = cur.getTimeInMillis();
        if(time < night.getTimeInMillis())
        	am.set(AlarmManager.RTC_WAKEUP, night.getTimeInMillis(), pendingIntent2);
        if(time < midday.getTimeInMillis())
        	am.set(AlarmManager.RTC_WAKEUP, midday.getTimeInMillis(), pendingIntent3);
        if(time < beginEat.getTimeInMillis())
        	am.set(AlarmManager.RTC_WAKEUP, beginEat.getTimeInMillis(), pendingIntent4);
        if(time < remindEat.getTimeInMillis())
        	am.set(AlarmManager.RTC_WAKEUP, remindEat.getTimeInMillis(), pendingIntent5);
        if(time < endEat.getTimeInMillis())
        	am.set(AlarmManager.RTC_WAKEUP, endEat.getTimeInMillis(), pendingIntent6);
        
    }
}
