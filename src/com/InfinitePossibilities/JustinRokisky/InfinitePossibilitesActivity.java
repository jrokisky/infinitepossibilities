package com.InfinitePossibilities.JustinRokisky;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This class is for the title screen.
 * @author justin
 *
 */
public class InfinitePossibilitesActivity extends Activity {
	public static final String TAG = "MyActivity";
	private boolean awake; //true if the awake button has been pressed
	private Button buttWaterInc; // increments water
	private ProgressBar waterProgress; // displays amount of water drank for day
	private int watersDrank; // tracks amount of water drank for day
	private Button buttHoneyInc; // increments honey
	private ProgressBar honeyProgress; //displays amount of honey eaten for day
	private int honeyEaten; //tracks amount of honey eaten
	private String cardColor;
	private TextView textviewCardColor;
	private TextView textviewNumDays;
	private Button buttSleep;
	private int numDays;
	private TextView textviewTypeDay;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("stats", 0);
        awake = settings.getBoolean("Wake", false);
        
        /**
         * If the app is "awake" for the day, show the daily screen.
         * The daily screen will include waters drank, next rotation, etc.
         * If not, show the "Wake" screen
         */
        if(awake) {
	        setContentView(R.layout.daily_screen);
	        
	        //Loads data from last use
	        watersDrank = settings.getInt("Waters", 0);
	        honeyEaten = settings.getInt("Honey", 0);
	        cardColor = settings.getString("Card", " ");
	        numDays = settings.getInt("Days", 1);
	        
	        textviewTypeDay = (TextView)findViewById(R.id.textView_typeofday);
	        if((numDays%4) == 0 || (numDays%4) == 3) {
	        	textviewTypeDay.setText("Milk");
	        } else {
	        	textviewTypeDay.setText("Fruit and Veggies");
	        }
	        
	        textviewCardColor = (TextView)findViewById(R.id.TextView_ColorCard);
	        textviewCardColor.setText(cardColor);
	        textviewNumDays = (TextView)findViewById(R.id.TextView_numofDays);
	        textviewNumDays.setText(Integer.toString(numDays));	        		
	        
	        waterProgress = (ProgressBar)findViewById(R.id.waterProgress);
	        waterProgress.setMax(8);
	        waterProgress.setProgress(watersDrank);
	        
	        honeyProgress = (ProgressBar)findViewById(R.id.honeyProgress);
	        honeyProgress.setMax(5);
	        honeyProgress.setProgress(honeyEaten);
	        
	        buttWaterInc = (Button)findViewById(R.id.button_inc_water);
	        buttWaterInc.setOnClickListener(new OnClickListener() { // start card chooser activity
	            public void onClick(View v) {
	                updateWaterBar();
	            }
	        });
	        
	        buttHoneyInc = (Button)findViewById(R.id.button_inc_honey);
	        buttHoneyInc.setOnClickListener(new OnClickListener() { // start card chooser activity
	            public void onClick(View v) {
	                updateHoneyBar();
	            }
	        });
	        
	        //Put the app to sleep, this will reset all daily values, i.e. water, honey
	        buttSleep = (Button)findViewById(R.id.button_sleep);
	        buttSleep.setOnClickListener(new OnClickListener() { // start card chooser activity
	            public void onClick(View v) {
	                goToSleep();
	            }
	        });
	        
        } else { //If the app isn't "awake" yet, send the user to the wake screen
        	Intent intent = new Intent(InfinitePossibilitesActivity.this, WakeActivity.class);
            startActivity(intent);
            finish();
        }

    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	SharedPreferences settings = getSharedPreferences("stats", 0);
    	Editor edit = settings.edit();
    	edit.clear();
    	edit.putBoolean("Wake", awake);
    	edit.putInt("Days", numDays);
    	edit.putInt("Waters", watersDrank);
    	edit.putInt("Honey", honeyEaten);
    	edit.putString("Card", cardColor);
    	edit.commit();
    }
    
    /**
     * Updates the water progress bar
     */
    private void updateWaterBar() {
    	if(watersDrank < 8) {
    		watersDrank++;
    		waterProgress.setProgress(watersDrank);
    		
    	}
    }
    
    /**
     * Updates the honey progress bar
     */
    private void updateHoneyBar() {
    	if(honeyEaten < 5) {
    		honeyEaten++;
    		honeyProgress.setProgress(honeyEaten);
    		
    	}
    }
    
    /**
     * Put the app to sleep.
     * Reset daily values
     */
    private void goToSleep() {
    	//Cancel the scheduled alarms
        Intent intent = new Intent(this, ChangeBeadsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
            1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
      //Pending intent for midday bead rotation
        Intent intent2 = new Intent(this, ChangeBeadsActivity.class);
        PendingIntent pendingIntent2 = PendingIntent.getActivity(this,
            2, intent2, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for night bead rotation
        Intent intent3 = new Intent(this, ChangeBeadsActivity.class);
        PendingIntent pendingIntent3 = PendingIntent.getActivity(this,
            3, intent3, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for begin Eat
        Intent intent4 = new Intent(this, BeginEatActivity.class);
        PendingIntent pendingIntent4 = PendingIntent.getActivity(this,
            4, intent4, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for remind Eat
        Intent intent5 = new Intent(this, RemindEatActivity.class);
        PendingIntent pendingIntent5 = PendingIntent.getActivity(this,
            5, intent5, PendingIntent.FLAG_CANCEL_CURRENT);
        
      //Pending intent for end Eat
        Intent intent6 = new Intent(this, EndEatActivity.class);
        PendingIntent pendingIntent6 = PendingIntent.getActivity(this,
            6, intent6, PendingIntent.FLAG_CANCEL_CURRENT);
        
        AlarmManager am =
            (AlarmManager)getSystemService(Activity.ALARM_SERVICE);
        am.cancel(pendingIntent);  
        am.cancel(pendingIntent2);   
        am.cancel(pendingIntent3);   
        am.cancel(pendingIntent4);   
        am.cancel(pendingIntent5);   
        am.cancel(pendingIntent6);   
        
        //Commit data to sharedPref
    	awake = false;
    	numDays++;
    	honeyEaten = 0; //need to zero these here ontherwise onPause will commit values over 
    	watersDrank = 0;
    	SharedPreferences settings = getSharedPreferences("stats", 0);
    	Editor edit = settings.edit();
    	edit.clear();
    	edit.putInt("Days", numDays);
    	edit.putBoolean("Wake", awake);
    	edit.putString("Card", cardColor);
    	edit.commit();
    	
    	//Print a Goodnight message
    	Context context = getApplicationContext();
    	CharSequence text = "Have a good night!";
    	int duration = Toast.LENGTH_SHORT;
    	Toast toast = Toast.makeText(context, text, duration);
    	toast.show();
    	finish();
    }
}




